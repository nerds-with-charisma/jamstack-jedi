import React from 'react';
import { PropTypes } from 'prop-types';

const PortfolioItem = ({ item, setPortItem } ) => (
  <figure className="brick">
    <button
      type="button"
      onClick={() => setPortItem(item)}
    >
      <img
        alt={item.alt}
        src={item.src}
      />
    </button>
  </figure>
);

PortfolioItem.propTypes = {
  item: PropTypes.object.isRequired,
  setPortItem: PropTypes.func.isRequired,
};

export default PortfolioItem;