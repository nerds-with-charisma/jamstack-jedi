import React from 'react';
import { PropTypes } from 'prop-types';
import { StaticQuery, graphql } from 'gatsby';

const SocialLinks = () => (
  <StaticQuery
    query={ graphql`
      query SocialQuery {
        site {
          siteMetadata{
            socialLinks {
              link
              icon
              title
            }
          }
        }
      }
    `}

    render={(data) => (
      <section className="social-links">
        { data.site.siteMetadata.socialLinks.map((link) => (
          <a
            key={link.title}
            className="font--grey padding-md"
            href={link.link} target="_blank"
            rel="noopener noreferrer"
          >
            <i className={`${link.icon} font--24`}>
            <div className="ir">
              {link.title}
            </div>
          </i>
        </a>
      ))}
    </section>
  )}
  />
);

export default SocialLinks;