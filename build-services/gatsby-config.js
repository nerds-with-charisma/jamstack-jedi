module.exports = {
  siteMetadata: {
    homepageData: {
      title: `Nerds With Charisma`,
      lang: 'en',
      meta: [
        {
          name: 'description',
          content: 'Nerds With Charisma online portfolio coming soon!',
        },
        {
          name: 'keywords',
          content: 'Brian Dausman, Nerds With Charisma',
        },
      ],
    },
    socialLinks: [
      {
        link: 'https://gitlab.com/nerds-with-charisma',
        icon: 'fab fa-gitlab',
        title: 'GitLab',
      },
      {
        link: 'https://twitter.com/nerdswcharisma',
        icon: 'fab fa-twitter',
        title: 'Twitter',
      },
      {
        link: 'https://www.npmjs.com/~nerds-with-charisma',
        icon: 'fab fa-npm',
        title: 'NPM',
      },
      {
        link: 'https://medium.com/@nerdswithcharisma',
        icon: 'fab fa-medium-m',
        title: 'Medium',
      },
    ],
    heroData: {
      letter: 'N',
      contactButtonText: 'Get In Touch',
      heroCopy: ['websites.', 'apps.', 'seo.', 'better.'],
      randomQuote: [
        'Coding Since Before We Could Walk.',
        'The Best in the World at What We Do.',
        'Super-Awesome Apps for Super-Awesome Peeps.',
        'I Think We\'re Gonna Need A Bigger Boat.',
        'Coding Ninjas.',
        'Punk Rock Pixels.',
        'The Best There Is, Was, and Ever Will Be.',
        'Do Or Do Not. There Is No Try.',
        'We Know "The Google".',
        'FOR SCIENCE!',
        'Reinforced by Space-Age Technology.',
      ],
    },
    servicesData: {
      letter: 'W',
      tagline: 'Here\'s just some of the stuff Nerds With Charisma can help you with',
      services: [
        {
          title: 'DEVELOPMENT',
          items: [
            'Full-Stack - Node / React / GraphQL',
            'JamStack',
            'Mobile App / React Native',
            'Wordpress Websites',
            'SEO & Marketing',
            'Analytics, Tagging, Tracking Pixels',
            'Backups & Monitoring',
            'eCommerce',
          ],
        },
        {
          title: 'DESIGN & SEO',
          items: [
            'Business Cards',
            'Logos & Branding',
            'Stationary',
            'E-Blasts',
            'Social & Strategy',
            'Keyword Research',
            'Analytics',
            'And More...',
          ],
        },
      ],
      logos: [
        {
          url: '/logos/nodejs.svg',
          gridSize: '4',
          title: 'Node JS',
        },
        {
          url: '/logos/react.svg',
          gridSize: '4',
          title: 'ReactJs & React Native',
        },
        {
          url: '/logos/graphql.svg',
          gridSize: '4',
          title: 'GraphQl & Prisma',
        },
        {
          url: '/logos/wordpress.svg',
          gridSize: '3',
          title: 'Wordpress Development',
        },
        {
          url: '/logos/google.svg',
          gridSize: '3',
          title: 'Google Analytics',
        },
        {
          url: '/logos/magento.svg',
          gridSize: '3',
          title: 'Magento',
        },
        {
          url: '/logos/photoshop.svg',
          gridSize: '3',
          title: 'Design / Adobe Creative Suite',
        },
        {
          url: '/logos/gitlab.svg',
          gridSize: '4',
          title: 'Consulting',
        },
        {
          url: '/logos/html5.svg',
          gridSize: '4',
          title: 'HTML5, Web Development',
        },
        {
          url: '/logos/css3.svg',
          gridSize: '4',
          title: 'CSS3, Design and UX',
        },
      ],
    },
    title: 'Nerds With Charisma',
    navigation: [
      'home',
      'services',
      'portfolio',
      'about',
      'contact',
    ],
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
  },
  plugins: [
    `gatsby-plugin-sass`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        // icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
