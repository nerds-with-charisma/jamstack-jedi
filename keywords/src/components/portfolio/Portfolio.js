import React, { useState, useEffect } from 'react';
import { PropTypes } from 'prop-types';
import LazyLoad from 'react-lazyload';

import PortfolioHeading from './portfolio-heading';
import PortfolioItem from './portfolio-item';
import PortfolioSingle from './portfolio-single';

const Portfolio = ({ portfolioData, currentProjectToOpen }) => {
  const [portItem, setPortItem] = useState(null);

  useEffect(() => {
    if (currentProjectToOpen) setPortItem(currentProjectToOpen);
  }, [currentProjectToOpen]);

  useEffect(() => {
      if (portItem) {
        window.history.pushState(null, null, `/project/${portItem.alt.toLowerCase().replace(/ /g, '-').toLowerCase()}`);
      } else {
        window.history.pushState(null, null, '/');
      }
    }, [portItem]);

  return (
    <section id="portfolio" className="position--relative overflow--container">
      <div className="col-12 text-center">
        <br />
        <br />
        <br />
        <PortfolioHeading tagline={portfolioData.tagline} />
      </div>

      <div className="masonry">
        <LazyLoad offset={400}>
          <div className="masonry">
            { portfolioData.portfolioData.sort((a, b) => a.sort < b.sort).map((item, i) => (
              <PortfolioItem key={`${item.alt}${i}`} item={item} setPortItem={setPortItem} />
            ))}
          </div>
        </LazyLoad>
      </div>

      <PortfolioSingle item={portItem} setPortItem={setPortItem} />
  </section>
  )
};

Portfolio.propTypes = {
  portfolioData: PropTypes.object.isRequired,
};

export default Portfolio;