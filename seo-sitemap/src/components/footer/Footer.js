import React from 'react';
import { PropTypes } from 'prop-types';

import SocialLinks from '../common/social-links';

const Footer = ({ title, signOff, footerTagline }) => (
  <section id="footer" className="container text-center padding-lg lh-md font--16">
    <strong>
      <div dangerouslySetInnerHTML={{ __html: signOff || '' }} />
    </strong>

    <br />
    <br />

    <SocialLinks />

    <br />
    <br />

    <small className="font--text">
      {`copyright © ${new Date().getFullYear()} ${title.toLowerCase()}`}
      <br />
      <span className="font--light">
        {footerTagline}
      </span>
    </small>
  </section>
);

Footer.propTypes = {
  title: PropTypes.string.isRequired,
  signOff: PropTypes.string.isRequired,
  footerTagline: PropTypes.string.isRequired,
};

export default Footer;