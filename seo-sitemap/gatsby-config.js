module.exports = {
  siteMetadata: {
    siteUrl: `https://nerdswithcharisma.com`,
    homepageData: {
      title: `Nerds With Charisma`,
      lang: 'en',
      meta: [
        {
          name: 'description',
          content: 'Nerds With Charisma online portfolio coming soon!',
        },
        {
          name: 'keywords',
          content: 'Brian Dausman, Nerds With Charisma',
        },
      ],
    },
    socialLinks: [
      {
        link: 'https://gitlab.com/nerds-with-charisma',
        icon: 'fab fa-gitlab',
        title: 'GitLab',
      },
      {
        link: 'https://twitter.com/nerdswcharisma',
        icon: 'fab fa-twitter',
        title: 'Twitter',
      },
      {
        link: 'https://www.npmjs.com/~nerds-with-charisma',
        icon: 'fab fa-npm',
        title: 'NPM',
      },
      {
        link: 'https://medium.com/@nerdswithcharisma',
        icon: 'fab fa-medium-m',
        title: 'Medium',
      },
    ],
    heroData: {
      letter: 'N',
      contactButtonText: 'Get In Touch',
      heroCopy: ['websites.', 'apps.', 'seo.', 'better.'],
      randomQuote: [
        'Coding Since Before We Could Walk.',
        'The Best in the World at What We Do.',
        'Super-Awesome Apps for Super-Awesome Peeps.',
        'I Think We\'re Gonna Need A Bigger Boat.',
        'Coding Ninjas.',
        'Punk Rock Pixels.',
        'The Best There Is, Was, and Ever Will Be.',
        'Do Or Do Not. There Is No Try.',
        'We Know "The Google".',
        'FOR SCIENCE!',
        'Reinforced by Space-Age Technology.',
      ],
    },
    servicesData: {
      letter: 'W',
      tagline: 'Here\'s just some of the stuff Nerds With Charisma can help you with',
      services: [
        {
          title: 'DEVELOPMENT',
          items: [
            'Full-Stack - Node / React / GraphQL',
            'JamStack',
            'Mobile App / React Native',
            'Wordpress Websites',
            'SEO & Marketing',
            'Analytics, Tagging, Tracking Pixels',
            'Backups & Monitoring',
            'eCommerce',
          ],
        },
        {
          title: 'DESIGN & SEO',
          items: [
            'Business Cards',
            'Logos & Branding',
            'Stationary',
            'E-Blasts',
            'Social & Strategy',
            'Keyword Research',
            'Analytics',
            'And More...',
          ],
        },
      ],
      logos: [
        {
          url: '/logos/nodejs.svg',
          gridSize: '4',
          title: 'Node JS',
        },
        {
          url: '/logos/react.svg',
          gridSize: '4',
          title: 'ReactJs & React Native',
        },
        {
          url: '/logos/graphql.svg',
          gridSize: '4',
          title: 'GraphQl & Prisma',
        },
        {
          url: '/logos/wordpress.svg',
          gridSize: '3',
          title: 'Wordpress Development',
        },
        {
          url: '/logos/google.svg',
          gridSize: '3',
          title: 'Google Analytics',
        },
        {
          url: '/logos/magento.svg',
          gridSize: '3',
          title: 'Magento',
        },
        {
          url: '/logos/photoshop.svg',
          gridSize: '3',
          title: 'Design / Adobe Creative Suite',
        },
        {
          url: '/logos/gitlab.svg',
          gridSize: '4',
          title: 'Consulting',
        },
        {
          url: '/logos/html5.svg',
          gridSize: '4',
          title: 'HTML5, Web Development',
        },
        {
          url: '/logos/css3.svg',
          gridSize: '4',
          title: 'CSS3, Design and UX',
        },
      ],
    },
    portfolioData: {
      tagline: 'Checkout some of the awesome websites and apps we\'ve worked on',
      "portfolioData": [
        {
          alt: 'Nerd Fit',
          src: '/portfolio/thumb--nerd-fit.jpg',
          type: 'Design, React Native Application',
          website: 'https://nerdswithcharisma.com',
          about: '<div>Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more.<br /><br />This was an amazing project to build, and probably the most fun we\'ve had on a project in a long time.<br /><br />NF will be in the appstore shortly.</div>',
          images: ['/portfolio/nerd-fit1.jpg', '/portfolio/nerd-fit3.jpg', '/portfolio/nerd-fit2.jpg'],
          hero: '//images.ctfassets.net/mdcl40cos6bh/2yHLzoHjK7xvfVjx40HGNn/213e974741f532f39d1efd73cc1e0092/hero--nerd-fit.jpg',
          tech: 'React Native, GraphQL',
          launchDate: '2019',
          sort: '1',
          browser: '//images.ctfassets.net/mdcl40cos6bh/38Ly2Ula6R16BK4qJ5szS8/e9697441edb790f74ed2e529b77f7241/nerd-fit3.jpg',
        },
        {
          alt: 'Nerd Fit',
          src: '/portfolio/thumb--nerd-fit.jpg',
          type: 'Design, React Native Application',
          website: 'https://nerdswithcharisma.com',
          about: '<div>Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more.<br /><br />This was an amazing project to build, and probably the most fun we\'ve had on a project in a long time.<br /><br />NF will be in the appstore shortly.</div>',
          images: ['/portfolio/nerd-fit1.jpg', '/portfolio/nerd-fit3.jpg', '/portfolio/nerd-fit2.jpg'],
          hero: '//images.ctfassets.net/mdcl40cos6bh/2yHLzoHjK7xvfVjx40HGNn/213e974741f532f39d1efd73cc1e0092/hero--nerd-fit.jpg',
          tech: 'React Native, GraphQL',
          launchDate: '2019',
          sort: '1',
          browser: '//images.ctfassets.net/mdcl40cos6bh/38Ly2Ula6R16BK4qJ5szS8/e9697441edb790f74ed2e529b77f7241/nerd-fit3.jpg',
        },
        {
          alt: 'Nerd Fit',
          src: '/portfolio/thumb--nerd-fit.jpg',
          type: 'Design, React Native Application',
          website: 'https://nerdswithcharisma.com',
          about: '<div>Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more.<br /><br />This was an amazing project to build, and probably the most fun we\'ve had on a project in a long time.<br /><br />NF will be in the appstore shortly.</div>',
          images: ['/portfolio/nerd-fit1.jpg', '/portfolio/nerd-fit3.jpg', '/portfolio/nerd-fit2.jpg'],
          hero: '//images.ctfassets.net/mdcl40cos6bh/2yHLzoHjK7xvfVjx40HGNn/213e974741f532f39d1efd73cc1e0092/hero--nerd-fit.jpg',
          tech: 'React Native, GraphQL',
          launchDate: '2019',
          sort: '1',
          browser: '//images.ctfassets.net/mdcl40cos6bh/38Ly2Ula6R16BK4qJ5szS8/e9697441edb790f74ed2e529b77f7241/nerd-fit3.jpg',
        },
        {
          alt: 'Nerd Fit',
          src: '/portfolio/thumb--nerd-fit.jpg',
          type: 'Design, React Native Application',
          website: 'https://nerdswithcharisma.com',
          about: '<div>Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more.<br /><br />This was an amazing project to build, and probably the most fun we\'ve had on a project in a long time.<br /><br />NF will be in the appstore shortly.</div>',
          images: ['/portfolio/nerd-fit1.jpg', '/portfolio/nerd-fit3.jpg', '/portfolio/nerd-fit2.jpg'],
          hero: '//images.ctfassets.net/mdcl40cos6bh/2yHLzoHjK7xvfVjx40HGNn/213e974741f532f39d1efd73cc1e0092/hero--nerd-fit.jpg',
          tech: 'React Native, GraphQL',
          launchDate: '2019',
          sort: '1',
          browser: '//images.ctfassets.net/mdcl40cos6bh/38Ly2Ula6R16BK4qJ5szS8/e9697441edb790f74ed2e529b77f7241/nerd-fit3.jpg',
        },
        {
          alt: 'Nerd Fit',
          src: '/portfolio/thumb--nerd-fit.jpg',
          type: 'Design, React Native Application',
          website: 'https://nerdswithcharisma.com',
          about: '<div>Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more.<br /><br />This was an amazing project to build, and probably the most fun we\'ve had on a project in a long time.<br /><br />NF will be in the appstore shortly.</div>',
          images: ['/portfolio/nerd-fit1.jpg', '/portfolio/nerd-fit3.jpg', '/portfolio/nerd-fit2.jpg'],
          hero: '//images.ctfassets.net/mdcl40cos6bh/2yHLzoHjK7xvfVjx40HGNn/213e974741f532f39d1efd73cc1e0092/hero--nerd-fit.jpg',
          tech: 'React Native, GraphQL',
          launchDate: '2019',
          sort: '1',
          browser: '//images.ctfassets.net/mdcl40cos6bh/38Ly2Ula6R16BK4qJ5szS8/e9697441edb790f74ed2e529b77f7241/nerd-fit3.jpg',
        },
        {
          alt: 'Nerd Fit',
          src: '/portfolio/thumb--nerd-fit.jpg',
          type: 'Design, React Native Application',
          website: 'https://nerdswithcharisma.com',
          about: '<div>Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more.<br /><br />This was an amazing project to build, and probably the most fun we\'ve had on a project in a long time.<br /><br />NF will be in the appstore shortly.</div>',
          images: ['/portfolio/nerd-fit1.jpg', '/portfolio/nerd-fit3.jpg', '/portfolio/nerd-fit2.jpg'],
          hero: '//images.ctfassets.net/mdcl40cos6bh/2yHLzoHjK7xvfVjx40HGNn/213e974741f532f39d1efd73cc1e0092/hero--nerd-fit.jpg',
          tech: 'React Native, GraphQL',
          launchDate: '2019',
          sort: '1',
          browser: '//images.ctfassets.net/mdcl40cos6bh/38Ly2Ula6R16BK4qJ5szS8/e9697441edb790f74ed2e529b77f7241/nerd-fit3.jpg',
        },
      ],
    },
    aboutData : {
      letter: 'C',
      who: {
        image: '/portfolio/brian.jpg',
        alt: 'That\'s me, Brian Dausman, founder of Nerds with Charisma',
        body: '<div>Nerds With Charisma are a small band of designers, developers, and creatives. Our unique process involves a hands on, back-and-forth approach to get your project off the ground and into the wild exactly as you envision it.<br /><br />Find out how we can bring fresh ideas and a new approach to your company. We don\'t just build your website, we build your brand and cultivate your online identity. We collaborate closely with you to learn your business, discover new opportunities, and bring your ideas to life.</div>',
      },
      what: {
        image: '/portfolio/andy.jpg',
        alt: 'That\'s Andrew Bieganski, he\'s been with with Nerds with Charisma for years!',
        body: '<div>We utilize the latest technologies & trends to get your site setup and rocking. Our help doesn\'t stop at just a website. Sure, we can help design & develop your site, but we will also discuss and explain your user\'s experience, come up with a content strategy for continued success, get you going on social media, explain the new technologies, go over analytics, and get your SEO juice flowing.<br /><br /></div>',
      },
      where: {
        image: '/portfolio/maeby.jpg',
        alt: 'That\'s our project manager and QA expert, Maeby, she\'s in charge of making sure we continue to make awesome websites.',
        body: 'We\'re located in the suburbs outside of Chicago, IL. We do lots of local work, but are totally willing to do long distance remote work, so long as you\'re ok with it. We\'re also very comfortable working with people pretty much anywhere in the world. We\'re previously worked with people in England, India, South America, and Israel.<br /><br />We also really enjoy doing websites for nonprofits. We can work with you and your budget to get your projects rolling. Simply shoot us an email below, and we\'ll let you know what we can do.',
      },
    },
    title: 'Nerds With Charisma',
    signOff: `
      <strong>
      Designed &amp; Developed By
      <br />
      <a href="/docs/brian-dausman-resume-2019.pdf">
        <h2 class="font--16 font--primary inline">
          <strong>brian dausman</strong>
        </h2>
      </a>
        +
      <a href="/docs/andrew-bieganski-resume-2019.pdf" target="_blank" rel="noopener noreferrer">
        <h2 class="font--16 font--primary inline">
          <strong>andrew bieganski</strong>
        </h2>
      </a>
    </strong>`,
      footerTagline: 'NWC making bitchin websites since the 90s!',
    navigation: [
      'home',
      'services',
      'portfolio',
      'about',
      'contact',
    ],
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
  },
  plugins: [
    `gatsby-plugin-sitemap`,
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: 'https://nerdswithcharisma.com',
        sitemap: 'https://nerdswithcharisma.com/sitemap.xml',
        policy: [{ userAgent: '*', allow: '/' }],
      },
    },
    `gatsby-plugin-sass`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        // icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: 'gatsby-plugin-google-tagmanager',
      options: {
        id: 'GTM-NP7BCNC',
        includeInDevelopment: true,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
