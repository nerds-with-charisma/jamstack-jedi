  // store that tag as a variable so we can effect it
  var dateElement = document.getElementById('copyrightDate');

  // get the current year and store it to a variable
  var currentYear = new Date().getFullYear();

  // replace the text of our dateElement with the current year
  dateElement.textContent = currentYear;


  // grab the h2 element and store it in a variable
  var rotatingTextElement = document.getElementById('rotatingText');

  // write an array of words to swap out
  var rotatingTextArray = ['Websites.', 'Apps.', 'SEO.', 'Development.', 'Mobile.'];

  // function for replacing our h2 content
  function replaceRotatingText() {
    // replace the h2 content with a random number from our array
    rotatingTextElement.textContent = rotatingTextArray[Math.floor(Math.random() * rotatingTextArray.length)];
  }

  // call the function on page load
  replaceRotatingText();

  // call the function every 3 seconds after page load
  setInterval(function() {
    replaceRotatingText();
  }, 3000);