import React from 'react';
import { PropTypes } from 'prop-types';
import { StaticQuery, graphql } from 'gatsby';

import HeroContact from './hero-contact';
import HeroHeading from './hero-heading';
import HeroQuote from './hero-quote';
import LargeLetter from '../common/large-letter';
import SocialLinks from '../common/social-links';

const Hero = ({ supportsWebP }) => (
  <StaticQuery
    query={ graphql`
      query HeroQuery {
        fileName: file(relativePath: { eq: "bg--hero.jpg" }) {
          childImageSharp {
            sizes(maxWidth: 1900, maxHeight: 1518) {
              src
              srcWebp
            }
          }
        }

        site {
          siteMetadata{
            heroData {
              letter
              contactButtonText
              heroCopy
              randomQuote
            }
          }
        }
      }
    `}
    render={(data) => (
      <section id="hero" className="font--light position--relative overflow--container" style={{
        background: `url(${(supportsWebP ? data.fileName.childImageSharp.sizes.srcWebp : data.fileName.childImageSharp.sizes.src)}) no-repeat center center fixed`
      }}>
        <LargeLetter letter={data.site.siteMetadata.heroData.letter} />
        <div>
          <HeroHeading
            heroCopy={data.site.siteMetadata.heroData.heroCopy}
          />
          <HeroQuote
            randomQuote={data.site.siteMetadata.heroData.randomQuote[Math.floor(Math.random() * data.site.siteMetadata.heroData.randomQuote.length)]}
          />
          <HeroContact
            contactButtonText={data.site.siteMetadata.heroData.contactButtonText}
          />
        </div>

        <SocialLinks />
      </section>
    )}
  />
);

Hero.propTypes = {
  supportsWebP: PropTypes.bool.isRequired,
};

export default Hero;