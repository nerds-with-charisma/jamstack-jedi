import React, { useState } from 'react';

const Contact = () => {
const [email, setEmail] = useState(null);
const [message, setMessage] = useState(null);
const [messageError, setMessageError] = useState(null);

const handleSubmit = (e) => {
  e.preventDefault();

  setMessageError(null);// remove the message each time they submit

  const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!regex.test(email)) {
    setMessageError('Please enter a valid e-mail.');
    return false;
  }

  if (!message || message.length < 2) {
    setMessageError('Please enter a valid message.');
    return false;
  }

  // todo: everything's good, we'll submit to mailchimp in a later chapter
  console.log(email, message);
};

return (
  <section id="contact" className="text-center bg--offLight padding-lg">
    <strong className="font--48">
      {'What can we help you with?'}
    </strong>
    <br />
    <br />

    { (messageError)
      && (
        <span class="bg--cta font--light padding-sm">
          { messageError }
        </span>
      )
    }

    <form id="contactForm" onSubmit={(e) => handleSubmit(e)}>
      <label htmlFor="email">
        <input
          type="email"
          id="email"
          name="email"
          placeholder="Your Email"
          onChange={(e) => setEmail(e.target.value)}
        />
      </label>
      <label htmlFor="message">
        <input
          type="message"
          id="message"
          name="message"
          placeholder="Your Message"
          onChange={(e) => setMessage(e.target.value)}
          />
      </label>

      <button type="submit" className="btn btn--gradient">
        {'Contact Us!'}
      </button>
    </form>
  </section>
)
};

export default Contact;