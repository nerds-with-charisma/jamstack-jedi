import React, { useState, useEffect } from 'react';
import { StaticQuery, graphql } from 'gatsby';

import Layout from "../components/layout";
import Header from '../components/header/Header';
import Hero from '../components/hero/Hero';
import SEO from '../components/seo';


const IndexPage = () => {
  const [supportsWebP, setSupportsWebP] = useState(true);

  // will run once, when the component mounts
  useEffect(() => {
    // check if we can use webP images
    setSupportsWebP(/Chrome/.test(window.navigator.userAgent) && /Google Inc/.test(window.navigator.vendor));
  }, []);

  return (
    <StaticQuery
      query={ graphql`
        query IndexQuery {
          site {
            siteMetadata {
              title
              homepageData {
                title
                lang
                meta {
                  name
                  content
                }
              }
            }
          }
        }
      `}
      render={(data) => (
        <Layout>
        <SEO
          title={data.site.siteMetadata.homepageData.title}
          lang={data.site.siteMetadata.homepageData.lang}
          meta={data.site.siteMetadata.homepageData.meta}
        />
        <Header title={data.site.siteMetadata.title} />
        <Hero supportsWebP={supportsWebP} />
      </Layout>
      )}
    />
  )
};

export default IndexPage;
