import React from 'react';
import { PropTypes } from 'prop-types';
import Logo from './Logo';
import Nav from './Nav';

const Header = ({ title }) => (
  <header>
    <Nav />
    <Logo title={title} />
  </header>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Header;