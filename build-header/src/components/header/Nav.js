import React from 'react';

const Nav = () => (
  <section id="headerNav">
    <button
      className="font--light"
      type="button"
    >
      <span>
        <i class="fas fa-bars">
          <span className="ir">
            Menu
          </span>
        </i>
      </span>
    </button>
  </section>
);

export default Nav;