import React from 'react';
import { StaticQuery, graphql } from 'gatsby';

const Hero = ({ supportsWebP }) => (
  <StaticQuery
    query={ graphql`
      query HeroQuery {
        fileName: file(relativePath: { eq: "bg--hero.jpg" }) {
          childImageSharp {
            sizes(maxWidth: 1900, maxHeight: 1518) {
              src
              srcWebp
            }
          }
        }
      }
    `}
    render={(data) => (
      <section id="hero" className="font--light" style={{
        background: `url(${(supportsWebP ? data.fileName.childImageSharp.sizes.srcWebp : data.fileName.childImageSharp.sizes.src)}) no-repeat center center fixed`
      }}>
        <span id="N">
          {'N'}
        </span>
        <div>
          <h2 className="font--125">
            {'websites.'}
            <br />
            {'apps.'}
            <br />
            {'seo.'}
            <br />
            {'better.'}
          </h2>
          <aside className="font--40">
            {'Quote placeholder'}
          </aside>
          <button
            type="button"
            className="btn bg--light font--dark font--24 padding-horiz-xl radius--lg border--none block no--wrap"
            onClick={() => alert('todo: scroll to footer')}
          >
            <strong>
              {'Get In Touch'}
            </strong>
          </button>
        </div>
      </section>
    )}
  />
);

export default Hero;