import React from 'react';
import { PropTypes } from 'prop-types';

import SocialLinks from '../common/social-links';

const Footer = ({
  title,
  signOff,
  footerTagline,
  coffeeUrl,
}) => (
  <section id="footer" className="container text-center padding-lg lh-md font--16">
    <strong>
      <div dangerouslySetInnerHTML={{ __html: signOff || '' }} />
    </strong>

    <br />
    <br />

    <SocialLinks />

    <br />
    <br />

    <small className="font--text">
      {`copyright © ${new Date().getFullYear()} ${title.toLowerCase()}`}
      <br />
      <span className="font--text opacity8">
        <div dangerouslySetInnerHTML={{ __html: footerTagline }} />
      </span>
    </small>

    <br />

    { coffeeUrl && (
      <a
        className="bmc-button"
        target="_blank"
        href={coffeeUrl}
        rel="noopener noreferrer"
      >
        <img
          src="https://bmc-cdn.nyc3.digitaloceanspaces.com/BMC-button-images/BMC-btn-logo.svg"
          alt="Buy me a coffee"
        />
        <span>
          {' Buy me a coffee'}
        </span>
      </a>
    )}
  </section>
);

Footer.defaultProps = {
  coffeeUrl: null,
};

Footer.propTypes = {
  coffeeUrl: PropTypes.string,
  title: PropTypes.string.isRequired,
  signOff: PropTypes.string.isRequired,
  footerTagline: PropTypes.string.isRequired,
};

export default Footer;
