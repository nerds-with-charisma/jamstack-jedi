import React from 'react';
import { PropTypes } from 'prop-types';
import Helmet from 'react-helmet';


import schema from '../config/schema-markup';

function SEO({
  title,
  lang,
  meta,
  og,
  googleMapsApiKey,
}) {
  return (
    <Helmet
      htmlAttributes={{ lang }}
      title={title}
    >

      <script
        async
        defer
        src={`https://maps.googleapis.com/maps/api/js?key=${googleMapsApiKey}`}
      />

      { meta.map((item) => (
        <meta key={item.name} name={item.name} content={item.content} />
      ))}

      { schema.data.map((item) => (
        <script key={item} type="application/ld+json">
          {JSON.stringify(item).replace(/_/g, '@')}
        </script>
      ))}

      { Object.keys(og).map((key) => (
        <meta property={`og:${key}`} content={og[key]} key={key} />
      ))}

      <link
        rel="alternate stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900"
        onLoad="this.rel='stylesheet'"
      />
      <link
        rel="alternate stylesheet"
        href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossOrigin="anonymous"
        onLoad="this.rel='stylesheet'"
      />
    </Helmet>
  );
}

SEO.defaultProps = {
  lang: 'en',
  googleMapsApiKey: null,
};

SEO.propTypes = {
  title: PropTypes.string.isRequired,
  lang: PropTypes.string,
  meta: PropTypes.array.isRequired,
  og: PropTypes.object.isRequired,
  googleMapsApiKey: PropTypes.string,
};

export default SEO;
