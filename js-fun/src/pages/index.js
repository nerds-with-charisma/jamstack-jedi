import React, { useState, useEffect } from 'react';
import { StaticQuery, graphql } from 'gatsby';

import Layout from "../components/layout";
import Header from '../components/header/Header';
import Hero from '../components/hero/Hero';
import SEO from '../components/seo';
import Services from '../components/services/Services';
import Portfolio from '../components/portfolio/Portfolio';
import About from '../components/about/About';
import Contact from '../components/contact/Contact';
import Footer from '../components/footer/Footer';

const IndexPage = ({ pageContext }) => {
  const [supportsWebP, setSupportsWebP] = useState(true);
    const [currentProjectToOpen, setCurrentProjectToOpen] = useState(null);
    const [city, setCity] = useState(null);

  // will run once, when the component mounts
  useEffect(() => {
    // check if we can use webP images
    setSupportsWebP(/Chrome/.test(window.navigator.userAgent) && /Google Inc/.test(window.navigator.vendor));

    // after the page load, lets check if we should load a portfolio item right away
    if (pageContext.currentProjectToOpen) setCurrentProjectToOpen(pageContext.currentProjectToOpen);

    // get the users lat/long
    const geolocation = new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        position => { resolve(position.coords) },
        error => { reject(error) }
      )
    }).catch(error => error);

    geolocation.then((data) => {
      setTimeout(() => {
        var geocoder = new window.google.maps.Geocoder();
        geocoder.geocode({'location': { lat: data.latitude, lng: data.longitude }}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {
              const city = results[0].address_components.filter((obj) => {
                return obj.types.includes('locality');
              })[0].long_name;

              setCity(city);
            }
          }
        });
      }, 3000);
    });

    // watch for mouse going towards the close button
    let mouseX = 0;
    let mouseY = 0;
    let hasShown = document.cookie.match(new RegExp('(^| )showLeavingMessage=([^;]+)'));  // set a flag by checking for cookie named "showLeavingMessage"

    const isMac = (window.navigator.platform === 'MacIntel');  // check if it's a mac, if not assume windows

    document.addEventListener('mousemove', (e) => {
      mouseX = e.clientX;
      mouseY = e.clientY;

      if (!hasShown) { // if we haven't done our thing yet, go ahead
        if (isMac === true && mouseX < 30 && mouseY < 30) { // assume mac, close is upper left
          console.log('woah mrs osx, don\'t leave');  // do your functionality here
          document.cookie = "showLeavingMessage=false"; // trigger our flag so next time it won't bother the user
        }

        if (isMac !== true && (mouseX < window.innerWidth - 30) && mouseY < 30) { // windows close is upper right
          console.log('easy mr windows, don\'t leave'); // do your functionality here
          document.cookie = "showLeavingMessage=false"; // trigger our flag so next time it won't bother the user
        }
      }
    });
  }, []);

  return (
    <StaticQuery
      query={ graphql`
        query IndexQuery {
          site {
            siteMetadata {
              coffeeUrl
              googleMapsApiKey
              title
              signOff
              footerTagline
              navigation
              homepageData {
                title
                lang
                meta {
                  name
                  content
                }
                og {
                  title
                  site_name
                  url
                  type
                  description
                  image
                }
              }

              servicesData {
                letter
                tagline
                services {
                  title
                  items
                }
                logos {
                  url
                  gridSize
                  title
                }
              }

              portfolioData {
                tagline
                portfolioData {
                  alt
                  src
                  type
                  website
                  about
                  images
                  hero
                  tech
                  launchDate
                  sort
                  browser
                }
              }

              aboutData {
                letter
                who {
                  image
                  alt
                  body
                }
                what {
                  image
                  alt
                  body
                }
                where {
                  image
                  alt
                  body
                }
              }
            }
          }
        }
      `}
      render={(data) => (
        <Layout>
        <SEO
          title={data.site.siteMetadata.homepageData.title}
          lang={data.site.siteMetadata.homepageData.lang}
          meta={data.site.siteMetadata.homepageData.meta}
          og={data.site.siteMetadata.homepageData.og}
          googleMapsApiKey={data.site.siteMetadata.googleMapsApiKey}
        />
        <Header title={data.site.siteMetadata.title} navigation={data.site.siteMetadata.navigation} />
        <Hero supportsWebP={supportsWebP} />
        <Services servicesData={data.site.siteMetadata.servicesData}
        />
        <Portfolio currentProjectToOpen={currentProjectToOpen} portfolioData={data.site.siteMetadata.portfolioData} />
        <About aboutData={data.site.siteMetadata.aboutData} city={city} />
        <Contact />
        <Footer
          title={data.site.siteMetadata.title}
          signOff={data.site.siteMetadata.signOff}
          footerTagline={data.site.siteMetadata.footerTagline}
          coffeeUrl={data.site.siteMetadata.coffeeUrl}
        />
      </Layout>
      )}
    />
  )
};

export default IndexPage;
