import React from 'react';
import { PropTypes } from 'prop-types';

const LargeLetter = ({ letter }) => (
  <span id={letter} className="large-letter">
    {letter}
  </span>
);

LargeLetter.propTypes = {
  letter: PropTypes.string.isRequired,
}

export default LargeLetter;