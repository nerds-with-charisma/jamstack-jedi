import React, { useState, useEffect } from 'react';
import { PropTypes } from 'prop-types';
import { StaticQuery, graphql } from 'gatsby';

import Layout from '../components/layout';

import Header from '../components/header/Header';
import Hero from '../components/hero/Hero';
import SEO from '../components/seo';
import Services from '../components/services/Services';
import Portfolio from '../components/portfolio/Portfolio';
import About from '../components/about/About';
import Contact from '../components/contact/Contact';
import Footer from '../components/footer/Footer';

const IndexPage = ({ pageContext }) => (
  <StaticQuery
    query={graphql`
      query IndexQuery {
        allContentfulPortfolio(sort: {fields: sort}) {
          edges {
            node {
              alt
                src
                type
                website
                about {
                  content {
                    content {
                      value
                    }
                  }
                }
                images
                hero
                tech
                launchDate
                sort
                browser
            }
          }
        }

        site {
          siteMetadata {
            useContentful
            coffeeUrl
            googleMapsApiKey
            title
            signOff
            footerTagline
            navigation
            homepageData {
              title
              lang
              meta {
                name
                content
              }
              og {
                title
                site_name
                url
                type
                description
                image
              }
            }
            servicesData {
              letter
              tagline
              services {
                title
                items
              }
              logos {
                url
                gridSize
                title
              }
            }
            portfolioData {
              tagline
              portfolioData {
                alt
                src
                type
                website
                about
                images
                hero
                tech
                launchDate
                sort
                browser
              }
            }
            aboutData {
              letter
              who {
                image
                alt
                body
              }
              what {
                image
                alt
                body
              }
              where {
                image
                alt
                body
              }
            }
          }
        }
      }
    `}
    render={
      (data) => {
        const [supportsWebP, setSupportsWebP] = useState(true);
        const [currentProjectToOpen, setCurrentProjectToOpen] = useState(null);
        const [city, setCity] = useState(null);

        // will run once, when the component mounts
        useEffect(() => {
          // check if we can use webP images
          setSupportsWebP(/Chrome/.test(window.navigator.userAgent) && /Google Inc/.test(window.navigator.vendor));

          // after the page load, lets check if we should load a portfolio item right away
          if (pageContext.currentProjectToOpen) {
            setCurrentProjectToOpen(pageContext.currentProjectToOpen);
          }

          // get the users lat/long
          const geolocation = new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(
              (position) => { resolve(position.coords); },
              (error) => { reject(error); },
            );
          }).catch((error) => error);

          geolocation.then((geo) => {
            setTimeout(() => {
              const geocoder = new window.google.maps.Geocoder();
              geocoder.geocode({
                location: {
                  lat: geo.latitude,
                  lng: geo.longitude,
                },
              }, (results, status) => {
                if (status === 'OK') {
                  if (results[0]) {
                    const cityFound = results[0].address_components.filter((obj) => obj.types.includes('locality'))[0].long_name;
                    setCity(cityFound);
                  }
                }
              });
            }, 3000);
          });

          // watch for mouse going towards the close button
          let mouseX = 0;
          let mouseY = 0;
          const hasShown = document.cookie.match(new RegExp('(^| )showLeavingMessage=([^;]+)')); // set a flag by checking for cookie named "showLeavingMessage"
          const isMac = (window.navigator.platform === 'MacIntel'); // check if it's a mac, if not assume windows

          document.addEventListener('mousemove', (e) => {
            mouseX = e.clientX;
            mouseY = e.clientY;

            if (!hasShown) { // if we haven't done our thing yet, go ahead
              if (isMac === true && mouseX < 30 && mouseY < 30) { // assume mac, close is upper left
                console.log('woah mrs osx, don\'t leave'); // do your functionality here
                document.cookie = 'showLeavingMessage=false'; // trigger our flag so next time it won't bother the user
              }

              if (isMac !== true && (mouseX < window.innerWidth - 30) && mouseY < 30) { // windows
                console.log('easy mr windows, don\'t leave'); // do your functionality here
                document.cookie = 'showLeavingMessage=false'; // trigger our flag so next time it won't bother the user
              }
            }
          });
        }, []);

        return (
          <Layout>
            <SEO
              googleMapsApiKey={data.site.siteMetadata.googleMapsApiKey}
              title={data.site.siteMetadata.homepageData.title}
              lang={data.site.siteMetadata.homepageData.lang}
              meta={data.site.siteMetadata.homepageData.meta}
              og={data.site.siteMetadata.homepageData.og}
            />
            <Header
              title={data.site.siteMetadata.title}
              navigation={data.site.siteMetadata.navigation}
            />
            <Hero supportsWebP={supportsWebP} />
            <Services servicesData={data.site.siteMetadata.servicesData} />
            <Portfolio
              currentProjectToOpen={currentProjectToOpen}
              portfolioData={data.site.siteMetadata.portfolioData}
              dynamicPortfolioData={data.allContentfulPortfolio.edges}
              useContentful={data.site.siteMetadata.useContentful}
            />
            <About aboutData={data.site.siteMetadata.aboutData} city={city} />
            <Contact />
            <Footer
              title={data.site.siteMetadata.title}
              signOff={data.site.siteMetadata.signOff}
              footerTagline={data.site.siteMetadata.footerTagline}
              coffeeUrl={data.site.siteMetadata.coffeeUrl}
            />
          </Layout>
        );
      }
    }
  />
);

IndexPage.propTypes = {
  pageContext: PropTypes.object.isRequired,
};

export default IndexPage;
