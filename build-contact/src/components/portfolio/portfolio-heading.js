import React from 'react';
import { PropTypes } from 'prop-types';

const PortfolioHeading = ({ tagline } ) => (
  <div className="col-12">
    <strong className="font--48">
      {'PORTFOLIO'}
    </strong>
    <br />
    {tagline}
    <br />
    <br />
    <br />
  </div>
);

PortfolioHeading.propTypes = {
  tagline: PropTypes.string.isRequired,
};

export default PortfolioHeading;