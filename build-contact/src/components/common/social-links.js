import React from 'react';
import { PropTypes } from 'prop-types';

const SocialLinks = ({ links }) => (
  <section className="social-links text-right">
    { links.map((link) => (
      <a key={link.title} className="font--grey" href={link.link} target="_blank">
        <i className={`${link.icon} font--24`}>
        <div className="ir">
          {link.title}
        </div>
        </i>
      </a>
    ))}
  </section>
);


SocialLinks.defaultProps = {
  links: [],
};

SocialLinks.propTypes = {
  links: PropTypes.array,
};

export default SocialLinks;