import React, { useState, useEffect } from 'react';
import { PropTypes } from 'prop-types';

import Drawer from './Drawer';

const Nav = ({ navigation }) => {
  const [isOpen, setIsOpen] = useState(false);

  const scrollToSection = (link) => {
    setIsOpen(false);
    console.log(link);
    document.getElementById(link).scrollIntoView({ behavior: 'smooth' });
  }

  return (
    <section id="headerNav">
      <button
        className="font--light"
        type="button"
        onClick={() => setIsOpen(!isOpen)}
      >
        <span>
          <i className={(isOpen) ? 'fas fa-times' : 'fas fa-bars'}>
            <span className="ir">
              Menu
            </span>
          </i>
        </span>
      </button>
      <Drawer isOpen={isOpen} navigation={navigation} scrollToSection={scrollToSection} />
    </section>
  )
};

Nav.propTypes = {
  navigation: PropTypes.array.isRequired,
};

export default Nav;