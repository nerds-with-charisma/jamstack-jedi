import React from "react"
import Helmet from "react-helmet"

function SEO({ title, lang, meta, og }) {
  return (
    <Helmet
      htmlAttributes={{ lang }}
      title={title}
      meta={meta}
    >
      { Object.keys(og).map((key, value) => (
        <meta property={`og:${key}`} content={og[key]} key={value} />
      ))}

      <link
        rel="alternate stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900"
        onload="this.rel='stylesheet'"
      />
      <link
        rel="alternate stylesheet"
        href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossOrigin="anonymous"
        onload="this.rel='stylesheet'"
      />
    </Helmet>
  )
}

export default SEO
