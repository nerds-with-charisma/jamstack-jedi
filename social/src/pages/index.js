import React, { useState, useEffect } from 'react';
import { StaticQuery, graphql } from 'gatsby';

import Layout from "../components/layout";
import Header from '../components/header/Header';
import Hero from '../components/hero/Hero';
import SEO from '../components/seo';
import Services from '../components/services/Services';
import Portfolio from '../components/portfolio/Portfolio';
import About from '../components/about/About';
import Contact from '../components/contact/Contact';
import Footer from '../components/footer/Footer';

const IndexPage = ({ pageContext }) => {
  const [supportsWebP, setSupportsWebP] = useState(true);
    const [currentProjectToOpen, setCurrentProjectToOpen] = useState(null);

  // will run once, when the component mounts
  useEffect(() => {
    // check if we can use webP images
    setSupportsWebP(/Chrome/.test(window.navigator.userAgent) && /Google Inc/.test(window.navigator.vendor));

    // after the page load, lets check if we should load a portfolio item right away
    if (pageContext.currentProjectToOpen) setCurrentProjectToOpen(pageContext.currentProjectToOpen);
  }, []);

  return (
    <StaticQuery
      query={ graphql`
        query IndexQuery {
          site {
            siteMetadata {
              title
              signOff
              footerTagline
              navigation
              homepageData {
                title
                lang
                meta {
                  name
                  content
                }
                og {
                  title
                  site_name
                  url
                  type
                  description
                  image
                }
              }

              servicesData {
                letter
                tagline
                services {
                  title
                  items
                }
                logos {
                  url
                  gridSize
                  title
                }
              }

              portfolioData {
                tagline
                portfolioData {
                  alt
                  src
                  type
                  website
                  about
                  images
                  hero
                  tech
                  launchDate
                  sort
                  browser
                }
              }

              aboutData {
                letter
                who {
                  image
                  alt
                  body
                }
                what {
                  image
                  alt
                  body
                }
                where {
                  image
                  alt
                  body
                }
              }
            }
          }
        }
      `}
      render={(data) => (
        <Layout>
        <SEO
          title={data.site.siteMetadata.homepageData.title}
          lang={data.site.siteMetadata.homepageData.lang}
          meta={data.site.siteMetadata.homepageData.meta}
          og={data.site.siteMetadata.homepageData.og}
        />
        <Header title={data.site.siteMetadata.title} navigation={data.site.siteMetadata.navigation} />
        <Hero supportsWebP={supportsWebP} />
        <Services servicesData={data.site.siteMetadata.servicesData}
        />
        <Portfolio currentProjectToOpen={currentProjectToOpen} portfolioData={data.site.siteMetadata.portfolioData} />
        <About aboutData={data.site.siteMetadata.aboutData} />
        <Contact />
        <Footer title={data.site.siteMetadata.title} signOff={data.site.siteMetadata.signOff} footerTagline={data.site.siteMetadata.footerTagline} />
      </Layout>
      )}
    />
  )
};

export default IndexPage;
