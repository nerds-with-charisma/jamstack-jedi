import React, { useState, useEffect } from 'react';
import { PropTypes } from 'prop-types';
import LazyLoad from 'react-lazyload';

import PortfolioHeading from './portfolio-heading';
import PortfolioItem from './portfolio-item';
import PortfolioSingle from './portfolio-single';

const Portfolio = ({ portfolioData, currentProjectToOpen }) => {
  const [item, setItem] = useState(null);

  useEffect(() => {
    // detect escape key and close the project if hit
    document.onkeydown = (e) => {
      console.log(e.keyCode === 27);
      if (e.keyCode === 27) {
        window.history.pushState(null, null, '/');
        setItem(null);
      }
    };
  }, []);

  useEffect(() => {
    if (currentProjectToOpen) setItem(currentProjectToOpen);
  }, [currentProjectToOpen]);

  useEffect(() => {
    if (item) {
      window.history.pushState(null, null, `/project/${item.alt.toLowerCase().replace(/ /g, '-').toLowerCase()}`);
    } else {
      window.history.pushState(null, null, '/');
    }
  }, [item]);

  return (
    <section id="portfolio" className="position--relative overflow--container">
      <div className="col-12 text-center">
        <br />
        <br />
        <br />
        <PortfolioHeading tagline={portfolioData.tagline} />
      </div>

      <LazyLoad offset={400}>
        <div className="masonry">
          { portfolioData.portfolioData.sort((a, b) => a.sort < b.sort).map((portItem) => (
            <PortfolioItem key={portItem.alt} item={portItem} setItem={setItem} />
          ))}
        </div>
      </LazyLoad>

      <PortfolioSingle item={item} setItem={setItem} />
    </section>
  );
};

Portfolio.defaultProps = {
  currentProjectToOpen: null,
};

Portfolio.propTypes = {
  portfolioData: PropTypes.object.isRequired,
  currentProjectToOpen: PropTypes.object,
};

export default Portfolio;
