import React from "react";
import { StaticQuery, graphql } from 'gatsby';

import Layout from "../components/layout";
import SEO from '../components/seo';


const IndexPage = () => (
  <StaticQuery
    query={ graphql`
      query IndexQuery {
        site {
          siteMetadata {
            homepageData {
              title
              lang
              meta {
                name
                content
              }
            }
          }
        }
      }
    `}
    render={(data) => (
      <Layout>
      <SEO
        title={data.site.siteMetadata.homepageData.title}
        lang={data.site.siteMetadata.homepageData.lang}
        meta={data.site.siteMetadata.homepageData.meta}
      />
      <h1>Hello World</h1>
    </Layout>
    )}
  />
);

export default IndexPage;
