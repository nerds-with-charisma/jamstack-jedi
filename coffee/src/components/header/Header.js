import React from 'react';
import { PropTypes } from 'prop-types';
import Logo from './Logo';
import Nav from './Nav';

const Header = ({ title, navigation }) => (
  <header>
    <Nav navigation={navigation} />
    <Logo title={title} />
  </header>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
  navigation: PropTypes.array.isRequired,
};

export default Header;