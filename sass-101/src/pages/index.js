import React from "react";

import Layout from "../components/layout";
import SEO from '../components/seo';


const IndexPage = () => (
  <Layout>
    <SEO
      title="Nerds With Charisma"
      lang="en"
      meta={[
        {
          name: 'description',
          content: 'Nerds With Charisma online portfolio coming soon!',
        },
        {
          name: 'keywords',
          content: 'Brian Dausman, Nerds With Charisma',
        },
      ]}
    />
    <h1>Hello World</h1>
  </Layout>
)

export default IndexPage
