import React from 'react';
  import { PropTypes } from 'prop-types';

  const PortfolioImages = ({ images } ) => (
    <section id="portfolioItem--images">
      { images.map((image) => (
        <img src={image} alt={image} key={image} />
      ))}
    </section>
  );

  PortfolioImages.propTypes = {
    images: PropTypes.array.isRequired,
  };

  export default PortfolioImages;