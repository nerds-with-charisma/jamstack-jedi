import React from 'react';
import { PropTypes } from 'prop-types';

const PortfolioItem = ({ item } ) => (
  <figure className="brick">
    <button
      type="button"
      onClick={() => alert('todo')}
    >
      <img
        alt={item.alt}
        src={item.src}
      />
    </button>
  </figure>
);

PortfolioItem.propTypes = {
  item: PropTypes.object.isRequired,
};

export default PortfolioItem;