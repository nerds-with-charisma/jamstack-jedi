import React from 'react';
import { PropTypes } from 'prop-types';

import PortfolioHero from './portfolio-hero';
import PortfolioType from './portfolio-type';
import PortfolioWhat from './portfolio-what';
import PortfolioImages from './portfolio-images';
import PortfolioFooter from './portfolio-footer';

const PortfolioSingle = ({ item } ) => (
  <aside
    id="portfolioSingle"
    className={(item !== null) ? 'active' : 'inactive'}
  >
    <button
      type="button"
      className="close"
      onClick={() => alert('todo')}
    >
      &times;
    </button>

    <PortfolioHero alt={item.alt} hero={item.hero} />
    <br />
    <br />
    <section className="container-md">
      <PortfolioType type={item.type} browser={item.browser} />
      <PortfolioWhat about={item.about} tech={item.tech} />
      <PortfolioImages images={item.images} />
      <PortfolioFooter website={item.website} launchDate={item.launchDate} />
    </section>
  </aside>
);

PortfolioFooter.defaultProps = {
  website: null,
  launchDate: null,
};

PortfolioFooter.propTypes = {
  website: PropTypes.string,
  launchDate: PropTypes.string,
};

export default PortfolioSingle;