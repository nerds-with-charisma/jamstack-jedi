import React from 'react';
import { PropTypes } from 'prop-types';

import PortfolioHeading from './portfolio-heading';
import PortfolioItem from './portfolio-item';
import PortfolioSingle from './portfolio-single';

const Portfolio = ({ portfolioData }) => (
  <section id="portfolio" className="position--relative overflow--container">
    <div className="col-12 text-center">
      <br />
      <br />
      <br />
      <PortfolioHeading tagline={portfolioData.tagline} />
    </div>

    <div className="masonry">
      { portfolioData.portfolioData.sort((a, b) => a.sort < b.sort).map((item, i) => (
          <PortfolioItem key={`${item.alt}${i}`} item={item} />
        ))
      }
    </div>

    <PortfolioSingle item={portfolioData.portfolioData[0]} />
  </section>
);

Portfolio.propTypes = {
  portfolioData: PropTypes.object.isRequired,
};

export default Portfolio;