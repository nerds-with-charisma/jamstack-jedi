import React from 'react';
import { PropTypes } from 'prop-types';

const ServicesLogos = ({ logos }) => {

  return (
  <div className="col-12 col-lg-4">
    <br />
    <div className="row grid text-center">
      { logos.map((logo) => {
        return (
          <div className={`col-${logo.gridSize}`} key={logo.title}>
            <img
              className="serviceLogo"
              width="50"
              src={logo.url}
              alt={logo.title}
            />
          </div>
        )
      })}
    </div>
  </div>
)};

ServicesLogos.defaultProps = {
  logos: PropTypes.array.isRequired,
};

export default ServicesLogos;