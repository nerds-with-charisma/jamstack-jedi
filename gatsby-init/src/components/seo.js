import React from "react"
import Helmet from "react-helmet"

function SEO({ title, lang, meta }) {
  return (
    <Helmet
      htmlAttributes={{ lang }}
      title={title}
      meta={meta}
    />
  )
}

export default SEO
